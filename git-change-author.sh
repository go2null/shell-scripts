#!/bin/sh

set -eu

# https://stackoverflow.com/questions/750172/how-do-i-change-the-author-and-committer-name-email-for-multiple-commits

# INPUT
# $1 - file with data in the following format.
# -------------------------------------
# OLD NAME;OLD_EMAIL; NEW NAME ; NEW_EMAIL
# OLD NAME;    -    ; NEW NAME ; NEW_EMAIL
# -       ;OLD_EMAIL; NEW NAME ; NEW_EMAIL
# -------------------------------------
# * Fields are separated by semicolons (`;`) in the file.
# * Spaces can be used to pad the values for readability
# * Use a `-` in OLD_NAME or OLD_EMAIL to ignore that value.
#   * For example, if the EMAIL is correct, but the OLD_NAME wrong.
#     In this case, if we check EMAIL, it will change all the commits
#     including those with NEW_NAME EMAIL.

main() {
	temp_script='/tmp/git-change-author-committer.sh'
	user_template='/tmp/git-change-author-committer_users.txt'
	get_branch

	get_file "$*"

	print_users
	inform_mapping
	write_script
	get_confirmation

	git switch --create "$backup_branch"
	git switch --create "$rebase_branch"
	git rebase --rebase-merges --root --exec "sh '$temp_script'" \
		|| rollback_changes
	print_users
}

get_branch() {
	source_branch="$(git rev-parse --abbrev-ref HEAD)"
	[ "$source_branch" = 'HEAD' ] && puts 'Invalid branch.' && exit 1

	datetime="$(date +'%Y%m%d-%H%M')"
	backup_branch="$source_branch-backup-$datetime"
	rebase_branch="$source_branch-rebase-$datetime"
}

get_file() {
	if [ -z "$1" ]; then
		puts 'Please specify the file with the data'
		generate_template
		exit 1
	fi

	[ -r "$1" ] && data_file="$1" && return

	puts 'Invalid file specified:'
	puts "  '$1'"
	generate_template
	exit 1
}

generate_template() {
	rm -rf "$user_template"
	git shortlog --summary --email \
		| sort -i -k2 \
		| while read -r _ name_email; do
		email="${name_email##* }"
		name="${name_email% *}"
		printf '%s ; %s ; %s ; %s\n' "$name" "$email" "$name" "$email" >> "$user_template"
	done

	echo
	puts 'Here is a template file to use.'
	puts '* Delete the lines that are good.'
	puts '* Change the user anme and email on the right to teh correct ones.'
	puts '* If only the name or email are incorect,'
	puts '  then replace the correct name or email on the left with a "-",.'
	puts "# $user_template"
	cat "$user_template"
	echo
}

print_users() {
	puts 'The current users are:'
	git shortlog --summary --email
}

inform_mapping() {
	echo
	puts 'Commits with the following Authors and/or Committers will be changed to'
}

# shellcheck disable=SC2016
write_script() {
	tee "$temp_script" <<-EOF >/dev/null
	#!/bin/sh

	set_author() {
		update=1
		author="--author='\$1 <\$2>'"
	}

	set_committer() {
		update=1
		export GIT_COMMITTER_NAME="\$1"
		export GIT_COMMITTER_EMAIL="\$2"
	}

	amend_commit() {
		git commit --amend --no-edit "\$@"
	}

	unset update

	auth_name="\$(git log -1 --pretty=format:"%an")"
	auth_mail="\$(git log -1 --pretty=format:"%ae")"
	comm_name="\$(git log -1 --pretty=format:"%cn")"
	comm_mail="\$(git log -1 --pretty=format:"%ce")"
	EOF

	backup_ifs
	while IFS=';' read -r old_name old_mail new_name new_mail; do
		old_name="$(trim "$old_name")"
		old_mail="$(trim "$old_mail")"
		new_name="$(trim "$new_name")"
		new_mail="$(trim "$new_mail")"
		printf '  %-25s%+25s\t%s\t%-25s%+25s\n' "$old_name" "$old_mail" '->' "$new_name" "$new_mail"

		# use double quote for *_name as name may include single-quote (ex: O'Lall)
		tee -a "$temp_script" <<-EOF >/dev/null

		[ "\$auth_name" = '$old_name' ] && set_author    "$new_name" '$new_mail'
		[ "\$auth_mail" = '$old_mail' ] && set_author    "$new_name" '$new_mail'
		[ "\$comm_name" = '$old_name' ] && set_committer "$new_name" '$new_mail'
		[ "\$comm_mail" = '$old_mail' ] && set_committer "$new_name" '$new_mail'
		EOF
	done < "$data_file"
	restore_ifs

	tee -a "$temp_script" <<-EOF >/dev/null

	[ -n "\$update" ] && amend_commit "\$author"
	git rebase --continue
	EOF
}

get_confirmation() {
	echo
	get_input 'Is the above mapping correct?' "Update '$data_file' and try again."

	echo
	puts 'This script will create a new branch and run the rebase there.'
	puts "  $rebase_branch"
	puts 'It will also create a backup of the original branch.'
	puts "  $backup_branch"
	puts 'You can review the script that will change the Authors and Committers.'
	puts "  $temp_script"
	puts 'To review the results, you can do the following:'
	puts '  git shortlog --summary --email'
	puts 'After review, you can accept the updated branch by doing the following:'
	puts "  git branch --move --force $source_branch"
	echo
	get_input 'Proceed?' 'User cancelled.'
}

rollback_changes() {
	git switch "$source_branch"
	git branch --delete "$backup_branch" "$rebase_branch"
	exit 1
} > /dev/null

get_input() {
	printf '%s\n' "$1 [Y/N]"
	read -r input
	if [ "$input" = 'Y' ] || [ "$input" = 'y' ]; then
		:
	else
		printf '%s\n' "$2"
		exit 1
	fi
}

# https://unix.stackexchange.com/questions/640062/how-to-temporarily-save-and-restore-the-ifs-variable-properly
backup_ifs() {
	unset old_ifs
	[ -n "${IFS+dummy}" ] && old_ifs=$IFS
}

restore_ifs() {
	unset IFS
	[ -n "${old_ifs+dummy}" ] && { IFS="old_ifs"; unset old_ifs; }
}

puts() { printf '%s\n' "$*"; }

trim() { trim_right "$(trim_left "$*")"; }

trim_left()  {
	text="$*"
	while [ "$text" != "${text# }" ]; do text="${text# }"; done
	puts "$text"
}

trim_right()  {
	text="$*"
	while [ "$text" != "${text% }" ]; do text="${text% }"; done
	puts "$text"
}

main "$@"
