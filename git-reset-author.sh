#!/bin/sh

set -eu

# https://stackoverflow.com/questions/750172/how-do-i-change-the-author-and-committer-name-email-for-multiple-commits

# INPUT parameters (both are optional)
# $1 - new name
# $2 - new email
# if not specified, defaults to user.name and user.email

main() {
	temp_script='/tmp/git-reset-author-committer'
	get_branch

	get_user "$@"

	print_users
	inform_mapping
	write_script
	get_confirmation

	git switch --create "$backup_branch"
	git switch --create "$rebase_branch"
	git rebase --rebase-merges --root --exec "sh '$temp_script'" \
		|| rollback_changes
	# The git rebase command above runs successfully.
	# However, it errors out after.
	# # -----------------------
	# Successfully rebased and updated refs/heads/rebase-main-20241126-1505.
	# error: could not open '.git/rebase-merge/git-rebase-todo': No such file or directory
	# warning: execution failed: sh '/tmp/git-reset-author-committer'
	# You can fix the problem, and then run
	#
	#   git rebase --continue
	# # -----------------------
	# The lines from `Warning:` onwards repeats
	print_users
}

get_branch() {
	source_branch="$(git rev-parse --abbrev-ref HEAD)"
	[ "$source_branch" = 'HEAD' ] && puts 'Invalid branch.' && exit 1

	datetime="$(date +'%Y%m%d-%H%M')"
	backup_branch="$source_branch-backup-$datetime"
	rebase_branch="$source_branch-rebase-$datetime"
}

get_user() {
	if [ "$#" -eq 2 ]; then
		new_name="$(trim "$1")"
		new_mail="$(trim "$2")"
	else
		new_name="$(git config user.name)"
		new_mail="$(git config user.email)"
	fi
}

print_users() {
	puts 'The current users are:'
 	git shortlog --summary --email
}

inform_mapping() {
	echo
	puts 'All commits will have their Author and Committer reset to'
	puts "  Name:  $new_name"
	puts "  Email: $new_mail"
}

# shellcheck disable=SC2016
write_script() {
	tee "$temp_script" <<-EOF >/dev/null
		#!/bin/sh

		set_author() {
		  update=1
		  author="--author='$new_name <$new_mail>'"
		}

		set_committer() {
		  update=1
		  export GIT_COMMITTER_NAME="$new_name"
		  export GIT_COMMITTER_EMAIL="$new_mail"
		}

		amend_commit() {
		  git commit --amend --no-edit "\$@"
		}

		unset update

		auth_name="\$(git log -1 --pretty=format:"%an")"
		auth_mail="\$(git log -1 --pretty=format:"%ae")"
		comm_name="\$(git log -1 --pretty=format:"%cn")"
		comm_mail="\$(git log -1 --pretty=format:"%ce")"

		[ "\$auth_name" != '$new_name' ] && set_author    "$new_name" '$new_mail'
		[ "\$auth_mail" != '$new_mail' ] && set_author    "$new_name" '$new_mail'
		[ "\$comm_name" != '$new_name' ] && set_committer "$new_name" '$new_mail'
		[ "\$comm_mail" != '$new_mail' ] && set_committer "$new_name" '$new_mail'

		[ -n "\$update" ] && amend_commit "\$author"
		git rebase --continue
	EOF
}

get_confirmation() {
	echo
	get_input 'Are the Name and Email correct?' 'Update the info and try again.'

	echo
	puts 'This script will create a new branch and run the rebase there.'
	puts "  $rebase_branch"
	puts 'It will also create a backup of the original branch.'
	puts "  $backup_branch"
	puts 'You can review the script that will change the Authors and Committers.'
	puts "  $temp_script"
	puts 'To review the results, you can do the following:'
	puts '  git shortlog --summary --email'
	puts 'After review, you can accept the updated branch by doing the following:'
	puts "  git branch --move --force $source_branch"
	echo
	get_input 'Proceed?' 'User cancelled.'
}

rollback_changes() {
	git switch "$source_branch"
	git branch --delete "$backup_branch" "$rebase_branch"
	exit 1
} > /dev/null

get_input() {
	printf '%s\n' "$1 [Y/N]"
	read -r input
	if [ "$input" = 'Y' ] || [ "$input" = 'y' ]; then
		:
	else
		printf '%s\n' "$2"
		exit 1
	fi
}

puts() { printf '%s\n' "$*"; }

trim() { trim_right "$(trim_left "$*")"; }

trim_left()  {
	text="$*"
	while [ "$text" != "${text# }" ]; do text="${text# }"; done
	puts "$text"
}

trim_right()  {
	text="$*"
	while [ "$text" != "${text% }" ]; do text="${text% }"; done
	puts "$text"
}

main "$@"
