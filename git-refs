#!/bin/sh

# version=0.2.20191024

# Default options
remotes='refs/remotes'
branches='refs/heads'
tags='refs/tags'
sort_by='-committerdate'
subject='%s %C(yellow)[%an]'
# descend into submodules of current repo
descend_submodules=1
# descend into submodules of submodules of this repo
recursive='--recursive'
unset is_recursion

# parse command-line overrides, keep params to pass on in recursion
counter=1
while [ "$counter" -le $# ]; do
	[ "$skip" ] && unset skip && continue
	case "$(eval "$counter")" in
		--no-remotes)            unset remotes            ;;
		--no-branches)           unset branches           ;;
		--no-tags)               unset tags               ;;
		--sort)                  sort_by=$2 && skip=1     ;;
		--no-subject)            unset subject            ;;
		--no-descend-submodules) unset descend_submodules ;;
		--no-recurse-submodules) unset recursive          ;;
		is_recursion)            is_recursion=1           ;;
	esac
	counter=$((counter + 1))
done

# colors
# text-decoration
T_NORMAL='\033[0m'
T_BOLD='\033[1m'

if [ "$is_recursion" ]; then
	printf '%b' "${T_NORMAL}"
else
	printf '%b' "${T_BOLD}%s${T_NORMAL}\n" "$PWD"
fi

# cannot pipe to 'sort --unique --reverse'
# as it will sort two commits on the same day by the hash,
# and so loose the date order
for ref in $(
	git  for-each-ref \
		--sort="$sort_by" \
		--format="%(objectname)" \
		"$remotes" "$branches" "$tags"
); do
	if [ "$prev_ref" != "$ref" ]; then
		prev_ref=$ref
		git log \
			-n1 "$ref" \
			--date=short \
			--pretty=format:"%C(auto)%ad %h%d $subject%C(reset)"
	fi
done

if [ "$descend_submodules" ]; then
	printf '%b' "\n${T_BOLD}"
	git submodule foreach $recursive "$0 is_recursion $*"
fi
