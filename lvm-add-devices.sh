#!/bin/sh

# == USER SETTINGS == #


# get params, using defaults if not present in environment
: ${KEY_DIR:='/root/luks'}
: ${KEY_FILE:="$KEY_DIR/keyfile"}
: ${LVM_LV:='data_lv'}
: ${LVM_VG:='data_vg'}
: ${PHY_DEVICES:='sdz'}


# == CODE BELOW - DO NOT CHANGE == #


main() {
	vet_root
	show_config
	vet_params
	vet_crypttab
	vet_keyfile
	create_pv "$PHY_DEVICES"
	resize_lv
	show_config
}


# abort if not run by root or sudo
vet_root() {
	[ "$USER" != 'root' ] && fatal 'Must run as `root`.'
}


show_config() {
	info ''
	lsblk -a -o NAME,SIZE,TYPE,FSTYPE,LABEL,MOUNTPOINT

	df -h

	# LVM status
	info '# pvscan #' && pvscan
	info '# vgscan #' && vgscan
	info '# lvscan #' && lvscan

	printf '\n%s\n' '# /etc/crypttab'
	cat '/etc/crypttab'
}


vet_params() {
	info ''
	info "KEY_DIR     = '$KEY_DIR'"
	info "KEY_FILE    = '$KEY_FILE'"
	info "LVM_LV      = '$LVM_LV'"
	info "LVM_VG      = '$LVM_VG'"
	info "PHY_DEVICES = '$PHY_DEVICES'"
	info ''

	info "Enter 'Y/y' to proceed, or any other key to abort: [$response]"
	read -r response

	if [ "$response" = 'Y' ] || [ "$response" = 'y' ]; then
		:
	else
		info 'User aborted.'
		exit
	fi
}


vet_crypttab() {
	touch           '/etc/crypttab'
	chown root:root '/etc/crypttab'
	chmod 0744      '/etc/crypttab'
}


vet_block_device() {
	info "# $PHY_DEV: Verify block device"
	[ -b "/dev/$1" ] || fatal "'/dev/$1' is an invalid block device."
}


vet_keyfile() {
	if [ ! -r "$KEY_FILE" ]; then
		try touch "$KEY_FILE"
		try dd bs=512 count=4  if='/dev/random' of="$KEY_FILE"
		try chmod a-rwx                            "$KEY_FILE"
	fi
}


create_pv() {
	for PHY_DEV in $*; do
		vet_block_device "$PHY_DEV"
		LUKS_DEV="${PHY_DEV}_crypt"
		encrypt_device
		add_keyfile
		add_to_boot
		add_to_vg
	done
}


encrypt_device() {
	info "# $PHY_DEV: Encrypt and map to '$LUKS_DEV'"
	try cryptsetup --cipher aes-xts-plain64 --hash sha512 --key-size 512 \
		--iter-time 5000 --use-random --verify-passphrase --verbose        \
		luksFormat            "/dev/$PHY_DEV"
	try cryptsetup luksOpen "/dev/$PHY_DEV" "$LUKS_DEV"
	cryptsetup status                       "$LUKS_DEV" # confirm active
}


add_keyfile() {
	info "# $PHY_DEV: Add keyfile '$KEY_FILE'"
	try cryptsetup luksClose                                                                          "$LUKS_DEV" # close so can
	try cryptsetup luksAddKey                                                         "/dev/$PHY_DEV" "$KEY_FILE" #   open after
	try cryptsetup luksOpen         --key-file           "$KEY_FILE"                  "/dev/$PHY_DEV" "$LUKS_DEV" #   key added
	try cryptsetup luksHeaderBackup --header-backup-file "$KEY_DIR/${PHY_DEV}.header" "/dev/$PHY_DEV"
	try cryptsetup luksDump         --key-file           "$KEY_FILE"                  "/dev/$PHY_DEV"
}


add_to_boot() {
	info "# $PHY_DEV: Map on boot"
	LUKS_UUID=$(cryptsetup luksUUID "/dev/$PHY_DEV")
	echo "$LUKS_DEV UUID=$LUKS_UUID $KEY_FILE" >> '/etc/crypttab'
}


add_to_vg() {
	info "# $PHY_DEV: Add to LVM VG"
	try pvcreate           "/dev/mapper/$LUKS_DEV"
	try vgextend "$LVM_VG" "/dev/mapper/$LUKS_DEV"
}


resize_lv() {
	try lvextend --verbose --alloc anywhere --extents +100%FREE "/dev/$LVM_VG/$LVM_LV"
	try resize2fs -p "/dev/mapper/$LVM_VG-$LVM_LV"
}


# == LIBRARY FUNCTIONS == #


fatal() {
	info "FATAL: $*"
	exit 1
}


info() {
	printf '%s\n' "$*"
}


try() {
	if $*; then
		:
	else
		fatal "The following command failed:
		$*"
	fi
}


main "$@"
